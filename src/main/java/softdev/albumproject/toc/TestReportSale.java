/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.albumproject.toc;

import java.util.List;
import softdev.albumproject.model.ReportSale;
import softdev.albumproject.service.ReportService;

/**
 *
 * @author focus
 */
public class TestReportSale {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();
        /*List<ReportSale> reportDay = reportService.getReportSaleByDay();
        for (ReportSale r : reportDay) {
            System.out.println(r);
        }*/

        List<ReportSale> reportMonth = reportService.getReportSaleByMonth(2013);
        for (ReportSale r : reportMonth) {
            System.out.println(r);
        }

        /*List<ReportSale> reportYear = reportService.getReportSaleByYear();
        for (ReportSale r : reportYear) {
            System.out.println(r);
        }*/
    }
}
