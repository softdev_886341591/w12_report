/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.albumproject.service;


import java.util.List;
import softdev.albumproject.dao.SaleDao;
import softdev.albumproject.model.ReportSale;

/**
 *
 * @author focus
 */
public class ReportService {
    public List<ReportSale> getReportSaleByDay(){
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }
    
    public List<ReportSale> getReportSaleByMonth(int year){
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }

    public List<ReportSale> getReportSaleByYear(){
        SaleDao dao = new SaleDao();
        return dao.getYearReport();
    }
}
